var htmlDecode = require('./entities').decode;
var htmlEncode = require('./entities').encode;

function Doc() {
	if(!(this instanceof Doc)) {
		throw new Error('Use Node as a class');
	}
	var header;
	this.__defineGetter__('header', function(){ return header; });
	this.__defineSetter__('header', function(node) {
		if(!node) {
			header = null;
		}
		if(!(node instanceof Node)) {
			throw new Error('Header must be a node');
		}
		if(node.tag !== '?xml') {
			throw new Error('Header must be a <?xml> node node');
		}
		header = node;
		node.parent = this;
	});

	var doctype;
	this.__defineGetter__('doctype', function(){ return doctype; });
	this.__defineSetter__('doctype', function(text) {
		if(!text) {
			doctype = null;
		}
		if(typeof text !== 'string') {
			throw new Error('Doctype must be a string');
		}
		doctype = text;
	});

	var values;
	this.__defineGetter__('value', function(){ return values; });

	this.clear = function() {
		header = null;
		doctype = null;
		values = [];
		values.toString = Doc.prototype.valueToString;
	};

	this.clear();
}

function Node(tag, attributes, value) {

	if(!(this instanceof Node)) {
		throw new Error('Use Node as a class');
	}

	if(typeof tag !== 'string') {
		throw new Error('Node needs a tag name');
	}
	this.__defineGetter__('tag', function(){ return tag; });

	if(typeof attributes !== 'object') {
		attributes = {};
	}
	this.__defineGetter__('attributes', function(){ return attributes; });

	var values = [];
	values.toString = Node.prototype.valueToString;
	this.__defineGetter__('value', function(){ return values; });

	if((value instanceof Comment) || (value instanceof Node) || (typeof value === 'string') || (typeof value === 'number')) {
		this.addChild(value);
	}
	else if(value instanceof Array){
		for(var i=0; i<value.length; i++) {
			this.addChild(value[i]);
		}
	}

	var parent = null;
	this.__defineGetter__('parent', function(){ return parent; });
	this.__defineSetter__('parent', function(node) {
		if(node && !(node instanceof Doc) && !(node instanceof Node)) {
			throw new Error('parent node is not a Node or Doc');
		}

		if(parent) {
			// remove from parent if present
			for(var i=0; i<parent.value.length; i++) {
				if(parent.value[i] === this) {
					parent.value.splice(i,1);
					break;
				}
			}
		}

		if(!node) {
			parent = null;
		} else {
			parent = node;
		}
	});
}

function Comment(text) {
	if(!this instanceof Comment) {
		throw new Error('Use Comment as a class');
	}

	this.comment = text || '<!-- -->';
}

function indent(level, type) {
	if(!type){ type = '\t'; }
	var tabs = '';
	for(var i=0; i<level; i++){ tabs += type; }
	return tabs;
}


Doc.prototype.toString = function(options) {
	if(!options) { options = {}; }

	if(options.multiLine !== false ) { options.multiLine = true; }
	if(!options.wrap) { options.wrap = 70; }
	if(!options.startIndent) { options.startIndent = 0; }

	var result = '';

	if(this.header) {
		result += this.header.toString(options, 0) + '\n';
	}
	if(this.doctype) {
		result += this.doctype + '\n';
	}

	result += this.value.toString(options, -1 - options.startIndent );

	return result;
};


Doc.prototype.valueToString = function(options, level, enclosed) {
	if(this.length === 0) {
		return null;
	}

	if(this.length === 1 && typeof this[0] === 'string') {
		return htmlEncode(this[0]);
	}

	var result = '';
	if(enclosed && options.multiLine) { result += '\n'; }
	for(var i=0; i<this.length; i++) {
		if(typeof this[i] === 'string') {
			result += indent(level + 1, options.indent);
			result += htmlEncode(this[0]);
			if(options.multiLine) { result += '\n'; }
		} else {
			result += this[i].toString(options, level+1);
			if(options.multiLine) { result += '\n'; }
		}
	}
	if(enclosed && options.multiLine) { result += indent(level, options.indent); }
	return result;
};
Node.prototype.valueToString = Doc.prototype.valueToString;


Comment.prototype.toString = function() {
	return this.comment;
};


Node.prototype.toString = function(options, level) {
	if(!options) { options = {}; }

	var result = '';

	// write out tags
	var line = '';
	if(options.multiLine) { line += indent(level, options.indent); }
	line += '<'+this.tag;
	var attributes = [];
	for(var key in this.attributes) {
		if(typeof this.attributes[key] === 'boolean') {
			attributes.push(key);
		} else {
			attributes.push(key + '="' + htmlEncode(this.attributes[key]) + '"');
		}
	}
	while(attributes.length) {
		var attrib = attributes.shift();
		if(options.multiLine && line.length+attrib.length+1 > options.wrap) {
			result += line+'\n';
			line = indent(level+1, options.indent)+attrib;
		} else {
			line += ' '+attrib;
		}
	}
	result += line;

	if(this.value.length === 0) {
		if(this.tag && this.tag[0] === '?') {
			result += '?>';
		} else {
			result += '/>';
		}
	}
	else {
		result += '>' + this.value.toString(options, level, true) + '</'+this.tag+'>';
	}

	return result;
};

Node.prototype.addChild = function(node) {
	return this.insertBefore(node);
};
Doc.prototype.addChild = Node.prototype.addChild;

Node.prototype.insertBefore = function(node, index) {
	if(typeof value === 'number') { node = node.toString(10); }
	if((node instanceof Comment) || (node instanceof Node) || (typeof node === 'string')) {
		if(typeof index !== 'number' || index < 0 || index >= this.value.length) {
			this.value.push(node);
		} else {
			this.value.splice(index,0,node);
		}
		node.parent = this;
		return this;
	}
	throw new Error('node is not a string or Node or Comment');
};
Doc.prototype.insertBefore = Node.prototype.insertBefore;

Node.prototype.match = function(part) {
	if(typeof part !== 'object') {
		return true;
	}

	if(part.tag !== undefined) {
		if(part.tag.toLowerCase() !== this.tag.toLowerCase()) {
			return false;
		}
	}

	if(part.attributes !== undefined) {
		var attributesFound = true;
		for(var j in part.attributes) {
			if(this.attributes[j] !== part.attributes[j]) {
				attributesFound = false;
				break;
			}
		}
		if(!attributesFound) { return false; }
	}

	if(part.value !== undefined) {
		if(!this.value || this.value.length !== 1 || this.value[0] !== part.value) {
			return false;
		}
	}

	return true;
};

/*
* results = node.find([ {tag: 'plist'}, {tag: 'dict'}, {} ]);
*/
Node.prototype.find = function(pathArray) {
	var results = [];

	if(!pathArray || !pathArray.length) { return []; }

	// non destructive pathArray.shift()
	var part = pathArray[0];
	pathArray = pathArray.slice(1);

	for(var i = 0; this.value && i<this.value.length; i++) {
		var child = this.value[i];

		if(child instanceof Node) {

			if(!child.match(part)) {
				continue;
			}

			if(pathArray.length) {
				results = results.concat(child.find(pathArray));
			} else {
				results.push(child);
			}

		}
	}
	return results;
};
Doc.prototype.find = Node.prototype.find;

Doc.prototype.parse = function(xml, options) {
	if(!(this instanceof Doc)) {
		var instance = new Doc();
		return instance.parse(xml, options);
	}

	var root = this;
	root.clear();

	if(!options) { options = {}; }

	var current = root;
	var child;

	while(xml.length) {
		if(!current) {
			throw new Error("Nesting error in xml");
		}

		var start = /^\s*<((\??)[a-z0-9_:-]+)/i.exec(xml);
		if(start) {
			xml = xml.substr(start[0].length);
			var tag = start[1];

			child = new Node(tag);

			if(tag === '?xml') {
				root.header = child;
			} else {
				current.addChild(child);
			}

			current = child;

			// eat attributes
			while(xml.length) {

				// end tag
				var startEnd = /^\s*(\/|\?|)>/i.exec(xml);
				if(startEnd) {
					xml = xml.substr(startEnd[0].length);
					if(current.tag[0] === '?' && startEnd[1] !== '?') {
						throw new Error("Invalid xml header");
					}
					if(startEnd[1]) {
						child = current;
						current = child.parent;
					}
					break;
				}

                var attrib;
                if( (attrib = /^\s*([a-z0-9_:-]+)\s*=\s*"([^"]*)"\s*/i.exec(xml)) ||
                    (attrib = /^\s*([a-z0-9_:-]+)\s*=\s*'([^']*)'\s*/i.exec(xml)) ||
				    (attrib = /^\s*([a-z0-9_:-]+)\s*=\s*([^>?\/\s]*)\s*/i.exec(xml))
                ) {
					xml = xml.substr(attrib[0].length);
                    var name = attrib[1];
                    var val = htmlDecode(attrib[2]);
					current.attributes[name] = val;
					continue;
				}

				var bool = /^\s*([a-z0-9_:-]+)\s*/i.exec(xml);
				if(bool) {
					xml = xml.substr(bool[0].length);
					current.attributes[bool[1]] = true;
					continue;
				}

				throw new Error("Unexpected content in tag: "+xml.substr(0,20)+"...");
			}
			continue;
		}

		var end = /^\s*<\/(\??[a-z0-9_-]+)>/i.exec(xml);
		if(end) {
			if(current.tag !== end[1]) {
				throw new Error("Incorrect xml end tab. Expecting </"+current.tag+"> but got </"+end[1]+">");
			}
			if(!options.emptyTags && current.value.length === 0) {
				current.value.push('');
			}
			xml = xml.substr(end[0].length);
			child = current;
			current = child.parent;
			continue;
		}

		var doctype = /^\s*(<!DOCTYPE.*?>)/.exec(xml);
		if(doctype) {
			xml = xml.substr(doctype[0].length);
			root.doctype = doctype[1];
			continue;
		}

		var comment = /^\s*<!((--)+)(\s|.)*?((--)+)>/.exec(xml);
		if(comment) {
			xml = xml.substr(comment[0].length);
			if(options.saveComments) {
				current.addChild(new Comment(comment[0].replace(/^\s*\r?\n/,'')));
			}
			continue;
		}

		var cdata = /^\s*<!\[CDATA\[((\s|.)*?)\]\]>/.exec(xml);
		if(cdata) {
			xml = xml.substr(cdata[0].length);
			current.addChild(cdata[1]);
			continue;
		}

		var value = /^([^<]*)</.exec(xml);
		if(value) {
			if(value[1].length === 0){
				throw new Error("Unexpected content inside tag: "+xml.substr(0,20)+"...");
			}
			xml = xml.substr(value[1].length);
			current.addChild(htmlDecode(value[1]));
			continue;
		}

		// end of file
		if(/\s*$/.exec(xml)) {
			break;
		}

		throw new Error("Unexpected content outside tag: "+xml.substr(0,20)+"...");

	}

	if(current !== root) {
		throw new Error("Unexpected end of xml. still in <"+current.tag+'>');
	}

	return root;
};

function xml2json(xml, options) {
	var instance = new Doc();
	instance.parse(xml, options);
	return instance;
}
function json2xml(root, options) {
	if(!(root instanceof Doc)) {
		throw new Error('root is not a Doc');
	}
	return root.toString(options);
}
function resolve(node, pathArray, results) {
	if(!(node instanceof Doc) && !(node instanceof Node)) {
		throw new Error('node is not a Node or Doc');
	}
	return node.find(pathArray);
}


module.exports = Doc;

module.exports.Doc = Doc;
module.exports.Node = Node;
module.exports.Comment = Comment;

	// don't use these
module.exports.xml2json = xml2json;
module.exports.json2xml = json2xml;
module.exports.resolve = resolve;



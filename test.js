
var xml =
'<?xml version="1.0" encoding="UTF-8" standalone="no"?>\r\n'+
'<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">\r\n'+
'<properties>\r\n'+
'	<entry key="application.iphonetargetname">BeautyAdvice</entry>\r\n'+
'	<entry key="application.publisherid">999</entry>\r\n'+
'	<entry key="android.min.sdk.version">8</entry>\r\n'+
'	<entry key="application.keywords"/>\r\n'+
'        <!-- this is a comment -->\r\n'+
'	<entry key="flurryapi.key"/>\r\n'+
'	<entry key="flurryapi.key.blackberry"/>\r\n'+
'	<entry key="application.ios-icon">app/res/artwork/icon.png</entry>\r\n'+
'	<entry key="application.ios-icon2x">app/res/artwork/icon@2x.png</entry>\r\n'+
'	<entry key="application.ios-icon72">app/res/artwork/icon-72.png</entry>\r\n'+
'	<entry key="application.ios-splash">app/res/artwork/splash.png</entry>\r\n'+
'	<entry key="application.ios-splash2x">app/res/artwork/splash@2x.png</entry>\r\n'+
'	<entry key="application.ios-itunesartwork">app/res/artwork/iTunesArtwork.png</entry>\r\n'+
'</properties>\r\n';


var xj = require('./xml2json');

var doc = xj.xml2json(xml, { saveComments: true});
console.log(doc.toString());

var newxml = xj.json2xml(doc, {wrap: 80});
console.log(newxml);

var doc = xj.xml2json(xml);

var nodes = doc.find([ {tag:'properties'}, {attributes:{key:'application.ios-icon'}} ]);
console.log('icon: '+nodes[0].value);

var nodes = doc.find([ {}, {value:'999'} ]);
console.log('publisher: '+nodes);

